import datetime
import re
import types
import urllib.request
import requests
from bs4 import BeautifulSoup
from Classes.Objects.Prof import Prof
from Classes.Tools import dbWriter
from Classes.Tools.dbWriter import addToMainList
from Classes.Tools.infoPageParser import getHTMLfromUrl

global evenBiggerHolder
evenBiggerHolder = []

def getAbrev(str):
    list = str.split(' ')
    print(list)
    print(len(list))


def stringPretify(str):
    aux = str.replace('\r',' ').replace('\t',' ').replace('\n',' ')
    aux = re.sub(' +',' ',aux)
    if aux.startswith(' '):
        aux = aux[1:]
    if aux.endswith(' '):
        aux = aux[:-1]
    return aux

def getTagAFromHTML(path):
    soup = BeautifulSoup(getHTMLfromUrl(path), 'html.parser')
    listOfA = []
    for elem in  soup.find_all("a"):
        title_search = re.search('<a href="(.*)">(.*)</a>', str(elem), re.IGNORECASE)
        if title_search:
            title = title_search.group(1)
            listOfA.append(title)
    return listOfA[5:]

def upperAndPretify(str):
    aux = stringPretify(str)
    if len(aux) > 2:
        return aux[0].upper() + aux[1:]
    else:
        return aux

def splittProfs(strg):
    aux = re.split('(Colab\.)|(Conf\.)|(Prof\.)|(drd\.)|(Lect\.)|(Asist\.)|(Cerc\.)', strg)
    list = [upperAndPretify(elem) for elem in aux if elem not in [None,'']]
    finalList = []
    if len(list) % 2 !=0:
        return list
        print(list)
    else:
        for i in range(0,len(list),2):
            finalList.append(list[i] + " " + list[i+1])
        return finalList

def classify(string, link):
    if string is None or link is None:
        return "idk"
    components = link.split("/")
    if len(components) < 5:
        return "idk"

    info = components[4]
    shit = components[5][5:]

    if info == "resurse":
        return ["room", string]
    if info == "discipline":
        return ["class", string]
    if shit.startswith("I") or shit.startswith("M"):
        name = shit.split('.')[0]
        if len(name) > 3:
            return ["group",name]
        else:
            return [None, None]
    if string.startswith("Reinmatriculari") or string.startswith("Informatica"):
        return [None,None]
    return ["prof",string]

def splittStuds(strg):
    aux = re.split(',', stringPretify(strg))
    list = [stringPretify(elem) for elem in aux if elem == elem]
    return list

def pasrseHTMLPage(path, secret):
    currentDay = ""
    column_names = []
    bigHolder = {}
    dayList = []

    soup = BeautifulSoup(getHTMLfromUrl(path), 'html.parser')
    description = soup.find("title")
    description1 = soup.find("h2")

    if (description is not None and description1 is not None) and len(description.text) < len(description1.text):
        description = description1

    auxsoup = BeautifulSoup(str(soup.find("tr")), 'html.parser')
    columns = auxsoup.findAll("th")

    for column in columns:
        column_names.append(stringPretify(str(column.text)).replace(' ','_').lower())
    shit = soup.find("table")

    auxName = stringPretify(description.text)[5:]

    type = classify(auxName, path)

    if type[0] is None and type[1] is None:
        return
    print("     > Working with : " + str(type[1]))
    bigHolder["type"] = type[0]
    bigHolder["name"] = type[1]
    bigHolder["link"] = path
    bigHolder["ref"] = "info"
    bigHolder["time"] = datetime.datetime.now().isoformat()
    if shit is None:
        return
    for col in shit:

        aux = BeautifulSoup(str(col), 'html.parser')
        help = aux.find("b")

        if help is not None:
            if len(dayList) > 0:
                bigHolder[currentDay] = dayList
                dayList = []
            currentDay = help.text
        else:
            daySchedule = aux.find_all("td")
            holder = {}
            lgh = min(len(column_names),len(daySchedule))
            for i in range(0,lgh-secret):
                if stringPretify(str(daySchedule[i].text)) != '\xa0':
                    if column_names[i] == "profesor":
                        holder[column_names[i]] = splittProfs(stringPretify(str(daySchedule[i].text)))
                    elif column_names[i] == "studenti":
                        holder[column_names[i]] = splittStuds(stringPretify(str(daySchedule[i].text)))
                    else:
                        holder[column_names[i]] = stringPretify(str(daySchedule[i].text))
            if holder != {}:
                dayList.append(holder)

    bigHolder[currentDay] = dayList
    evenBiggerHolder.append(bigHolder)


def parseParticipanti():
    path = "https://profs.info.uaic.ro/~orar/participanti/"
    participanti = getTagAFromHTML(path)

    for page in participanti:
        pasrseHTMLPage(path + page, 2)


def parseResurse():
    path = "https://profs.info.uaic.ro/~orar/resurse/"
    resurse = getTagAFromHTML(path)
    for page in resurse:
        pasrseHTMLPage(path + page, 1)


def parseDiscipline():
    path = "https://profs.info.uaic.ro/~orar/discipline/"
    discipline = getTagAFromHTML(path)
    for page in discipline:
        pasrseHTMLPage(path + page, 2)

def addToDB(what, client):
    if what == 'all':
        dbWriter.write(evenBiggerHolder,client)
        return True
    if type(what) is not list:
        what = [what]
    whatToAdd = [elem for elem in evenBiggerHolder if elem["type"] in what]
    #dbWriter.write(whatToAdd, client)
    return True

def startJob(client):
    parseParticipanti()
    parseResurse()
    parseDiscipline()
    addToDB('all', client)
    #doc_ref = client.collection(u'faculties').document(u'info').collection(u'teachers')
    #docs = doc_ref.get()

    # roList = []
    # for doc in docs:
    #     roList.append(doc.id)

    print(evenBiggerHolder)
    #addToMainList(client, "faculties", "info", "teachers", roList)
    #pasrseHTMLPage("https://profs.info.uaic.ro/~orar/participanti/orar_I1A1.html",2)
