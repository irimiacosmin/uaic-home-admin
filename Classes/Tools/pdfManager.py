import os
import re
import urllib.request
import PyPDF2
import io


import warnings
warnings.filterwarnings("ignore")

from Classes.Tools.infoPageParser import classes





def downloadPdf(ForceDownload,link):
    filenameList = link.split('/')
    if os.path.isfile("pdfs/"+filenameList[len(filenameList)-1]) and ForceDownload is False:
        return "pdfs/"+filenameList[len(filenameList)-1]
    with urllib.request.urlopen(link) as response:
        html = response.read()
        f = open("pdfs/"+filenameList[len(filenameList)-1], "wb")
        f.write(html)
        f.close()
        return "pdfs/"+filenameList[len(filenameList)-1]

def parsePDF(filepath, className):
    print("                  >   Collecting data from file ("+filepath+") about class(" + className + ")")
    pdfFileObj = open(filepath, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    document = []
    for i in range (0, pdfReader.numPages):
        page = pdfReader.getPage(i)
        text = page.extractText().replace('\n',' ')
        l = re.compile("\d\.").split(text)
        document.append(l)
    for muie in document:
            print(muie)

def getPDFContent(path, name):
    content = ""
    # Load PDF into pyPDF
    pdfFileObj = open(path, 'rb')

    pdf = PyPDF2.PdfFileReader(pdfFileObj)
    # Iterate pages
    for i in range(0, pdf.getNumPages()):
        # Extract text from page and add to content
        content += pdf.getPage(i).extractText() + "\n"
    # Collapse whitespace
    content = " ".join(content.replace(u"\xa0", " ").strip().split())
    l = re.compile("\d\.").split(content)
    print(l)

def callParseScript(path, name):
    list = os.system("pdf2txt.py "+path)



def start(ForceDownload=False):
    print("\n     > Starting PDF Manager<")
    for myclass in classes:
        if myclass.pdfLink is not None:
            print("         >  PDFManager: PDF.GET for class(" + myclass.name+")")
            val = downloadPdf(ForceDownload, myclass.pdfLink)
            if val is not None:
                callParseScript(val,myclass.name)
        else:
            print("         >  PDFManager: NULL.PDF for class(" + myclass.name + ")")
    print("     > Exiting PDF Manager<\n")

