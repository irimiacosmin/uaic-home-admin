import re
import urllib

import requests
from bs4 import BeautifulSoup

from Classes.Objects.Class import Class
from urllib.request import urlopen
import ssl

global classes
classes = []

def getHTMLfromUrl(url):
    response = urlopen(url, context=ssl._create_unverified_context())

    return response

def normalizeMaterie(str):
    a = str.split(': ')
    if len(a) > 1:
        return a[1].replace('\r','')
    else:
        return a[0]

def appendClass(name, link, year, sem):
    if len(link) > 0:
        classes.append(Class(name=name, pdfLink=link[0], year=year, semester=sem))
    else:
        classes.append(Class(name=name, year=year, semester=sem))

def startJob():
    print("\n     > Starting Webpage Parser <")
    soup = BeautifulSoup(getHTMLfromUrl("https://www.info.uaic.ro/bin/Programs/Undergraduate?language=ro"), 'html.parser')
    year = "0"
    sem = "0"
    for materie in soup.find_all("td"):
        tag = str(materie)[4:5]
        lungime = len(str(materie))
        if re.match("[a-zA-Z<]",tag) and lungime > 12:
            if lungime > 14 and lungime <17:
                year = str(materie.getText())[2:3]
                sem = str(materie.getText())[3:4]
            else:
                matList = str(materie.getText()).split('\n')
                auxList = str(materie).split('\n')
                if len(matList) > 1:
                   for i in range(1,len(matList)-1):
                       link = re.findall('http://.{1,}\.pdf', auxList[i])
                       appendClass(normalizeMaterie(matList[i]), link, year, sem)
                       print("          WebpageParser: catched("+normalizeMaterie(matList[i])+")")


                else:
                    link = re.findall('http://.{1,}\.pdf', auxList[0])
                    appendClass(matList[0],link,year,sem)
                    print("          WebpageParser: catched(" + matList[0] + ")")
    print("     > Exiting Webpage Parser <\n")