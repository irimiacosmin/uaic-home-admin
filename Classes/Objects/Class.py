class Class:
    def __init__(self, name, pdfLink=None, year=None, semester=None):
        self.name = name
        self.pdfLink = pdfLink
        self.year = year
        self.semester = semester

    def setOrar(self, orar):
        self.orar = orar

    def __str__(self) -> str:
        if self.pdfLink is not None:
            return "Class( name=\""+self.name+"\", year=\""+self.year+"\", semester=\""+self.semester+"\", pdfLink=\""+self.pdfLink+"\" )"
        else:
            return "Class( name=\""+self.name+"\", year=\""+self.year+"\", semester=\""+self.semester+"\" )"

