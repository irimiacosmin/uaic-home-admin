import hashlib


def md5MyString(str):
    return hashlib.md5(str.encode('utf-8')).hexdigest()