import firebase_admin
from firebase_admin import firestore, credentials

from Classes.Tools import infoPageParser, pdfManager, orarParser


class Rescan:
    def __init__(self, client, permissions):
        self.permissions = permissions
        self.client = client

    def start(self):
        print("> Starting Rescan Module <")
        # done parsing info.uaic and creating classes
        #infoPageParser.startJob()

        # done downloading and all pdfs
        orarParser.startJob(self.client)