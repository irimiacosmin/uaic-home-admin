import re

from pip._vendor.distlib.compat import raw_input
from pathlib import Path, PureWindowsPath
import sys
import firebase_admin
from firebase_admin import credentials, firestore
from Classes.Constants import Methods
from Classes.Engines.DBManager import DBManager
from Classes.Engines.Rescan import Rescan
from Classes.Engines.Suggestions import Suggestions
import Classes.Engines

def firebaseINI(path):
    if path is None:
        cred = credentials.Certificate("res/uaic-home-firebase-adminsdk-cd2ak-86e593e4cf.json")
    else:
        cred = credentials.Certificate(path)
    firebase_admin.initialize_app(cred)
    global client
    client = firestore.client()



def printOptions():
    print("\n Please choose an option:\n ")
    print("1. Manage App Database")
    print("2. Create suggestions")
    print("3. Rescan all shits")
    print("0. Close application")
    print("\nPlease input option number bellow:")


#users_ref = client.collection(u'adminSDK')
#user_doc = users_ref.document(u'irimia_cosmin').get()
#docs = users_ref.get()

#print(u'{} => {}'.format(user_doc.id, user_doc.to_dict()))
# for doc in docs:
#    print(u'{} => {}'.format(doc.id, doc.to_dict()))


def validateSecurityFile(filepath):
    my_file = Path(filepath)
    if my_file.is_file():
        file = open(filepath, "r")
        text = file.read()
        list = text.split("][")

        if Methods.md5MyString("homeuaicauth") == list[0][1:] and Methods.md5MyString("homeuaicauth") == list[len(list)-1][:-1]:
            if list[1].startswith(Methods.md5MyString("document_id")) and list[1].endswith(Methods.md5MyString("document_id")):
                document_id = re.search('\](.*)\[', list[1]).group(1)
            else:
                return "INVALID AUTH FILE"

            if list[2].startswith(Methods.md5MyString("irelevant_key")) and list[2].endswith(Methods.md5MyString("irelevant_key")):
                invalid_key = re.search('\](.*)\[', list[2]).group(1)
            else:
                return "INVALID AUTH FILE"

            if list[3].startswith(Methods.md5MyString("name")) and list[3].endswith(Methods.md5MyString("name")):
                name = re.search('\](.*)\[', list[3]).group(1)
            else:
                return "INVALID AUTH FILE"

            if list[4].startswith(Methods.md5MyString("username")) and list[4].endswith(Methods.md5MyString("username")):
                username = re.search('\](.*)\[', list[4]).group(1)
            else:
                return "INVALID AUTH FILE"

        else:
            return "INVALID AUTH FILE"
        try:
            users_ref = client.collection(u'adminSDK').document(username).get().to_dict()
            if name == users_ref.get("name") and invalid_key == users_ref.get(
                    "irelevant_key") and document_id == users_ref.get("document_id"):
                global user_permissions
                user_permissions = users_ref.get("permissions")

                return "Done"
            else:
                print("INVALID AUTH FILE")
        except Exception as detail:
            return "INVALID AUTH FILE"


    else:
        return "The provided path does not provide a valid auth file."


def option1():
   print(1)

def option2():
    if user_permissions in ["777","007","077"]:
        print("works")
    else:
        return "You do not have the permissions to execute this. Please contact app admin for further informations."

def option3():
    if user_permissions == "777":
        print("works")
    else:
        return "You do not have the permissions to execute this. Please contact app admin for further informations."


def work(option):
    if option is -1:
        printOptions()
        return False
    if option is 0:
        print("\n######################### HomeUAIC Admin Application ##########################")
        print("################################### BYE BYE ###################################")
        sys.exit()

    while True:
        print("This procedure is strictly classified. For further operations, please provide path of your auth file.")
        #text = raw_input("> ")
        text = "res/irimia_cosmin.ukkey"
        message = validateSecurityFile(text)
        if message is "Done":
            print("> Authentication succesfully")
            break
        else:
            print(message)

    if option is 1:
        DBManager(client, user_permissions).start()
        return True
    if option is 2:
        Suggestions(client, user_permissions).start()
        return True
    if option is 3:
        Rescan(client, user_permissions).start()
        return True



firebaseINI(None)
print("\n######################### HomeUAIC Admin Application ##########################")
#printOptions()
Rescan(client,"777").start()

while True:
    break # SECURITY REMOVE
    text = raw_input("> ")
    if text in ["-1","0","1","2","3"]:
        message = work(int(text))
        if message == True:
            break
    else:
        print("This command does not exists. Please use command -1 to show all the available options")

